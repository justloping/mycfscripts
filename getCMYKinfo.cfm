<cfscript>
	currPath = getDirectoryFromPath(getCurrentTemplatePath());
	imgFile = currPath & 'cmyktest.jpg';
	ioObj = createobject('java', 'javax.imageio.ImageIO');
	readers = ioObj.getImageReadersByFormatName('JPEG');

	while (readers.hasNext())
	{
		reader = readers.next();
		if (reader.canReadRaster())
			break;
	}

	fobj = createobject('java', 'java.io.File').init(imgFile);
	isObj = ioObj.createImageInputStream(fobj);
	reader.setInput(isObj);
	raster = reader.readRaster(0, JavaCast('null', ''));
	writeoutput('width&height: ' & raster.getWidth() & 'x' & raster.getHeight() & ' size: ' & fobj.length()/1024);
</cfscript>